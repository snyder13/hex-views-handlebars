// @flow
'use strict';
const hbs = require('handlebars');
const layouts = require('handlebars-layouts');
const fs = require('fs');
const path = require('path');
const watch = require('node-watch');

import type { Context } from 'express-hex/types/hex';

const loadLayouts = (viewPaths) => {
	const compiledLayouts = {};
	viewPaths.forEach((dir) => {
		if (!fs.existsSync(`${ dir }/layouts`)) {
			return;
		}
		fs.readdirSync(`${ dir }/layouts`).forEach((file) => {
			if (!/[.]hbs$/.test(file)) {
				return;
			}
			const name = path.basename(file, '.hbs');
			if (!compiledLayouts[name]) {
				hbs.registerPartial(name, fs.readFileSync(`${ dir }/layouts/${ file }`, { 'encoding': 'utf8' }));
				compiledLayouts[name] = true;
			}
		});
	});
};

module.exports = ({ app, conf }: Context) => {
	app.locals.hbs = hbs;

	const isDev = (/^dev/i).test(conf.get('env'));

	hbs.registerHelper(layouts(hbs));

	const cache = {};
	const getTemplate = (tplPath) => {
		if (isDev) {
			return hbs.compile(fs.readFileSync(tplPath, { 'encoding': 'utf8' }));
		}
		if (!cache[tplPath]) {
			cache[tplPath] = hbs.compile(fs.readFileSync(tplPath, { 'encoding': 'utf8' }));
		}
		return cache[tplPath];
	};

	const viewPaths = conf.get('paths.views', []);
	loadLayouts(viewPaths);
	if (isDev) {
		viewPaths.forEach((viewPath) => {
			watch(viewPath, {
				'persistent': false,
				'recursive': true,
				'filter': /[\/\\]layouts[\/\\].*?[.]hbs/
			}, () => loadLayouts(viewPaths))
		});
	}

	app.engine('hbs', (tplPath, opts, cb) => {
		return cb(null, getTemplate(tplPath)(opts));
	});
	app.set('view engine', 'hbs');
}
