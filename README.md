# express-views-handlebars

[express-hex](https://bitbucket.org/snyder13/express-hex)

## Installation

 * `yarn add express-views-handlebars` or `npm install --save express-views-handlebars`

## Usage

 * Add `hex-views-handlebars.engine` to `deps` in your middleware definition for any that use handlebars views:

`middleware.js`
```
module.exports = {
	'my-app': {
		'description': 'cool thing',
		'deps': [ 'hex-views-handlebars.engine' ]
	}
};
```

 * Add `.hbs` files to `views/` in your app. You can use [handlebars-layouts](https://github.com/shannonmoeller/handlebars-layouts) if you want to, as in this example:

`views/layouts/html.hbs`
```
<!DOCTYPE html>
<html>
	<head>
		<title>{{#block "title"}}{{/block}}</title>
	</head>
	<body>
		{{#block "body"}}{{/block}}
	</body>
</html>
```

`views/app.hbs`
```
{{#extend "html"}}
	{{#content "title"}}Greeting{{/content}}
	{{#content "body"}}
		<p>Hello, {{name}}</p>
	{{/content}}
{{/extend}}
```

 * Call `res.render('path/to/view', templateData)` in your middleware:

`middleware/my-app.js`
```
module.exports = ({ app }) => {
	app.get('/', (req, res) => {
		res.render('app', {
			'name': req.query.name ? req.query.name : 'world'
		});
	});
}
```

This middleware attaches the Handlebars instance to `app.locals.hbs`. You can use this to `registerHelper` or use any of the other Handlebars API and have those additions reflected in the behavior of your templates.
