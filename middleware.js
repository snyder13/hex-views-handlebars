'use strict';
// @flow
import type { MiddlewareDefs } from 'express-hex/types/hex';

const mw: MiddlewareDefs = module.exports = {
	'engine': {
		'description': 'Template engine for `views/*.hbs`'
	}
};
